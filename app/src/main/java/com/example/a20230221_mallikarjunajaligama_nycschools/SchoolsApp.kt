package com.example.a20230221_mallikarjunajaligama_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolsApp : Application()