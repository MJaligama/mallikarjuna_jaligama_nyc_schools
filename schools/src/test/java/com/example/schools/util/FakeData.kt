package com.example.schools.util

import com.example.schools.models.api_models.SchoolDetailsApiModel
import com.example.schools.models.api_models.SchoolDetailsApiModelItem
import com.example.schools.models.api_models.Schools
import com.example.schools.models.api_models.SchoolsItem
import com.example.schools.models.domain_models.DomainSchoolDetails
import com.example.schools.models.domain_models.DomainSchoolItem

val fakeApiSchoolDetails = SchoolDetailsApiModel().apply {
    add(
        SchoolDetailsApiModelItem(
            dbn = "dbn",
            num_of_sat_test_takers = "totalNumberOfTestTakers",
            sat_critical_reading_avg_score = "averageReadingScore",
            sat_math_avg_score = "averageMathScore",
            sat_writing_avg_score = "averageWritingScore",
            school_name = "schoolName"
        )
    )
}

val fakeDomainSchoolDetails = DomainSchoolDetails(
    dbn = "dbn",
    schoolName = "schoolName",
    totalNumberOfTestTakers = "totalNumberOfTestTakers",
    averageReadingScore = "averageReadingScore",
    averageMathScore = "averageMathScore",
    averageWritingScore = "averageWritingScore"
)

val fakeApiSchoolsList: Schools = Schools().apply {
    add(
        SchoolsItem(
            dbn = "dbn",
            school_name = "schoolName",
            overview_paragraph = "overviewParagraph",
            phone_number = "phoneNumber",
            school_email = "schoolEmail",
            state_code = "stateCode",
            total_students = "totalStudents",
            transfer = "transfer",
            website = "website",
            zip = "zip"
        )
    )
}

val fakeDomainSchoolsList = listOf(
    DomainSchoolItem(
        dbn = "dbn",
        schoolName = "schoolName",
        overviewParagraph = "overviewParagraph",
        phoneNumber = "phoneNumber",
        schoolEmail = "schoolEmail",
        stateCode = "stateCode",
        totalStudents = "totalStudents",
        transfer = "transfer",
        website = "website",
        zip = "zip"
    )
)