package com.example.schools.repository.network

import com.example.schools.util.fakeApiSchoolDetails
import com.example.schools.util.fakeApiSchoolsList
import com.example.schools.util.fakeDomainSchoolDetails
import com.example.schools.util.fakeDomainSchoolsList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class SchoolApiToDomainMapperTest {

    class TestObjects {
        val subject: SchoolApiToDomainMapper = SchoolApiToDomainMapper()
    }

    @Test
    fun `mapSchoolListData returns list of DomainSchoolItem`() {
        // Arrange
        val testObjects = TestObjects()

        // Act
        val result = testObjects.subject.mapSchoolListData(fakeApiSchoolsList)

        // Assert
        assertEquals(fakeDomainSchoolsList, result)
    }

    @Test
    fun `mapSchoolDetailsData returns DomainSchoolDetails`() {
        // Arrange
        val testObjects = TestObjects()

        // Act
        val result = testObjects.subject.mapSchoolDetailsData(fakeApiSchoolDetails)

        // Assert
        assertEquals(fakeDomainSchoolDetails, result)
    }
}