package com.example.schools.repository

import com.example.schools.util.ApiResult
import com.example.schools.repository.network.SchoolApiToDomainMapper
import com.example.schools.repository.network.SchoolsApiService
import com.example.schools.util.*
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class SchoolsRepositoryImplTest {

    @get:Rule
    val testCoroutineRule = CoroutineTestRule()

    class TestObjects {
        val mockSchoolsApiService: SchoolsApiService = mockk()
        val mockMapper: SchoolApiToDomainMapper = mockk()
        val subject: SchoolsRepositoryImpl = SchoolsRepositoryImpl(mockSchoolsApiService, mockMapper)
    }

    @Test
    fun `getSchoolsList returns list of DomainSchoolItem if apiService and mapping are successful`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolsList() } returns fakeApiSchoolsList
        coEvery { testObjects.mockMapper.mapSchoolListData(fakeApiSchoolsList) } returns fakeDomainSchoolsList

        // Act
        val result = testObjects.subject.getSchoolsList()

        // Assert
        assertTrue(result is ApiResult.Success)
        assertEquals(fakeDomainSchoolsList, (result as ApiResult.Success).value)
    }

    @Test
    fun `getSchoolsList returns error if apiService returns error`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolsList() } throws Exception("Error")

        // Act
        val result = testObjects.subject.getSchoolsList()

        // Assert
        assertTrue(result is ApiResult.Failure)
    }

    @Test
    fun `getSchoolsList returns error if mapping returns error`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolsList() } returns fakeApiSchoolsList
        coEvery { testObjects.mockMapper.mapSchoolListData(fakeApiSchoolsList) } throws Exception("Error")

        // Act
        val result = testObjects.subject.getSchoolsList()

        // Assert
        assertTrue(result is ApiResult.Failure)
    }

    @Test
    fun `getSchoolsList returns error if mapping returns empty list`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolsList() } returns fakeApiSchoolsList
        coEvery { testObjects.mockMapper.mapSchoolListData(fakeApiSchoolsList) } returns emptyList()

        // Act
        val result = testObjects.subject.getSchoolsList()

        // Assert
        assertTrue(result is ApiResult.Failure)
        assertEquals("empty response", (result as ApiResult.Failure).error)
    }

    @Test
    fun `getSchoolDetails returns DomainSchoolDetails if apiService and mapping are successful`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolDetails("dbn") } returns fakeApiSchoolDetails
        coEvery { testObjects.mockMapper.mapSchoolDetailsData(fakeApiSchoolDetails) } returns fakeDomainSchoolDetails

        // Act
        val result = testObjects.subject.getSchoolDetails("dbn")

        // Assert
        assertTrue(result is ApiResult.Success)
        assertEquals(fakeDomainSchoolDetails, (result as ApiResult.Success).value)
    }

    @Test
    fun `getSchoolDetails returns error if apiService returns error`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolDetails("dbn") } throws Exception("Error")

        // Act
        val result = testObjects.subject.getSchoolDetails("dbn")

        // Assert
        assertTrue(result is ApiResult.Failure)
    }

    @Test
    fun `getSchoolDetails returns error if mapping returns error`() = runTest {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockSchoolsApiService.getSchoolDetails("dbn") } returns fakeApiSchoolDetails
        coEvery { testObjects.mockMapper.mapSchoolDetailsData(fakeApiSchoolDetails) } throws Exception("Error")

        // Act
        val result = testObjects.subject.getSchoolDetails("dbn")

        // Assert
        assertTrue(result is ApiResult.Failure)
    }
}