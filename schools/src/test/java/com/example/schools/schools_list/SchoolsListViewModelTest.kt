package com.example.schools.schools_list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.schools.models.domain_models.DomainSchoolItem
import com.example.schools.repository.SchoolsRepository
import com.example.schools.util.ApiResult
import com.example.schools.util.CoroutineTestRule
import com.example.schools.util.fakeDomainSchoolsList
import com.example.schools.util.getOrAwaitValue
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class SchoolsListViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = CoroutineTestRule()

    class TestObjects(result: ApiResult<List<DomainSchoolItem>>) {
        val mockRepository: SchoolsRepository = mockk()
        val subject: SchoolsListViewModel

        init {
            coEvery { mockRepository.getSchoolsList() } returns result
            subject = SchoolsListViewModel(mockRepository)
        }
    }

    @Test
    fun `init fetches schools list from repository and returns Success SchoolsListState when successful`() {
        // Arrange & Act
        val testObjects = TestObjects(ApiResult.Success(fakeDomainSchoolsList))

        // Assert
        assertTrue(testObjects.subject.schoolsList.getOrAwaitValue() is SchoolsListState.Success)
        assertEquals(fakeDomainSchoolsList, (testObjects.subject.schoolsList.getOrAwaitValue() as SchoolsListState.Success).list)
        coVerify { testObjects.mockRepository.getSchoolsList() }
    }

    @Test
    fun `init fetches schools list from repository and returns Error SchoolsListState when unsuccessful`() {
        // Arrange & Act
        val testObjects = TestObjects(ApiResult.Failure("Error"))

        // Assert
        assertTrue(testObjects.subject.schoolsList.getOrAwaitValue() is SchoolsListState.Failure)
        assertEquals("Fetching schools failed! error: Error", (testObjects.subject.schoolsList.getOrAwaitValue() as SchoolsListState.Failure).message)
        coVerify { testObjects.mockRepository.getSchoolsList() }
    }

    @Test
    fun `init SchoolsListState is set to Empty when repository returns empty list`() {
        // Arrange & Act
        val testObjects = TestObjects(ApiResult.Success(emptyList()))

        // Assert
        assertTrue(testObjects.subject.schoolsList.getOrAwaitValue() is SchoolsListState.Empty)
        coVerify { testObjects.mockRepository.getSchoolsList() }
    }
}