package com.example.schools.school_details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.schools.util.ApiResult
import com.example.schools.repository.SchoolsRepository
import com.example.schools.util.CoroutineTestRule
import com.example.schools.util.fakeDomainSchoolDetails
import com.example.schools.util.getOrAwaitValue
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class SchoolTestDetailsViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = CoroutineTestRule()

    class TestObjects {
        val mockRepository: SchoolsRepository = mockk()
        val subject: SchoolTestDetailsViewModel = SchoolTestDetailsViewModel(mockRepository)
    }

    @Test
    fun `schoolDetails default state is Loading`() {
        // Arrange
        val testObjects = TestObjects()

        // Act
        val result = testObjects.subject.schoolDetails.getOrAwaitValue()

        // Assert
        assert(result is SchoolDetailsState.Loading)
    }

    @Test
    fun `getSchoolDetails returns Success SchoolDetailsState when repository returns school details successfully`() {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockRepository.getSchoolDetails(any()) } returns ApiResult.Success(
            fakeDomainSchoolDetails
        )

        // Act
        testObjects.subject.getSchoolDetails("1")

        // Assert
        assertTrue(testObjects.subject.schoolDetails.getOrAwaitValue() is SchoolDetailsState.Success)
        assertEquals(fakeDomainSchoolDetails, (testObjects.subject.schoolDetails.getOrAwaitValue() as SchoolDetailsState.Success).schoolDetails)
    }

    @Test
    fun `getSchoolDetails returns Error SchoolDetailsState when repository returns an error`() {
        // Arrange
        val testObjects = TestObjects()
        coEvery { testObjects.mockRepository.getSchoolDetails(any()) } returns ApiResult.Failure("Error")

        // Act
        testObjects.subject.getSchoolDetails("1")

        // Assert
        assertTrue(testObjects.subject.schoolDetails.getOrAwaitValue() is SchoolDetailsState.Failure)
    }
}