package com.example.schools.models.domain_models

data class DomainSchoolDetails(
    val dbn: String,
    val schoolName: String,
    val totalNumberOfTestTakers: String,
    val averageReadingScore: String,
    val averageMathScore: String,
    val averageWritingScore: String
)