package com.example.schools.models.api_models

data class SchoolsItem(
    val dbn: String,
    val school_name: String,
    val overview_paragraph: String?,
    val phone_number: String?,
    val school_email: String?,
    val state_code: String?,
    val total_students: String?,
    val transfer: String?,
    val website: String?,
    val zip: String?
)