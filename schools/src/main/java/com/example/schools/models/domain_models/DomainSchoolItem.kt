package com.example.schools.models.domain_models

data class DomainSchoolItem(
    val dbn: String,
    val schoolName: String,
    val overviewParagraph: String,
    val phoneNumber: String,
    val schoolEmail: String,
    val stateCode: String,
    val totalStudents: String,
    val transfer: String,
    val website: String,
    val zip: String
)