package com.example.schools.school_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.schools.models.domain_models.DomainSchoolDetails
import com.example.schools.repository.SchoolsRepository
import com.example.schools.util.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolTestDetailsViewModel @Inject constructor(
    private val schoolsRepository: SchoolsRepository
) : ViewModel() {

    private val _schoolDetails = MutableStateFlow<SchoolDetailsState>(SchoolDetailsState.Loading())
    val schoolDetails: LiveData<SchoolDetailsState>
        get() = _schoolDetails.asLiveData()

    fun getSchoolDetails(id: String) {
        viewModelScope.launch {
            _schoolDetails.value = SchoolDetailsState.Loading()
            when (val result = schoolsRepository.getSchoolDetails(id)) {
                is ApiResult.Success -> _schoolDetails.value = SchoolDetailsState.Success(result.value)
                is ApiResult.Failure -> _schoolDetails.value = SchoolDetailsState.Failure("Failed to load school details! error: ${result.error}")
            }
        }
    }
}

sealed class SchoolDetailsState() {
    class Loading : SchoolDetailsState()
    data class Success(val schoolDetails: DomainSchoolDetails) : SchoolDetailsState()
    data class Failure(val message: String) : SchoolDetailsState()
}