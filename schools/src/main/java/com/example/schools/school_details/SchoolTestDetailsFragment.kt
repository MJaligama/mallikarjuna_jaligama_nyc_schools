package com.example.schools.school_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.schools.R
import com.example.schools.databinding.FragmentSchoolTestDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolTestDetailsFragment : Fragment() {

    private lateinit var binding: FragmentSchoolTestDetailsBinding

    private val viewModel: SchoolTestDetailsViewModel by viewModels()
    private val args: SchoolTestDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolTestDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().title = getString(R.string.school_details_fragment_title)
        viewModel.getSchoolDetails(args.schoolId)
        viewModel.schoolDetails.observe(viewLifecycleOwner) {
            it?.let { bindState(it) }
        }
    }

    private fun bindState(state: SchoolDetailsState) {
        when (state) {
            is SchoolDetailsState.Loading -> {
                setDetailsVisibility(View.GONE)
                binding.progressBar.visibility = View.VISIBLE
                binding.errorMessage.visibility = View.GONE
            }
            is SchoolDetailsState.Success -> {
                binding.school = state.schoolDetails
                setDetailsVisibility(View.VISIBLE)
                binding.progressBar.visibility = View.GONE
                binding.errorMessage.visibility = View.GONE
            }
            is SchoolDetailsState.Failure -> {
                setDetailsVisibility(View.GONE)
                binding.progressBar.visibility = View.GONE
                binding.errorMessage.visibility = View.VISIBLE
            }
        }
    }

    private fun setDetailsVisibility(visibility: Int) {
        binding.schoolName.visibility = visibility
        binding.totalNumberOfTestTakers.visibility = visibility
        binding.averageMathScore.visibility = visibility
        binding.averageReadingScore.visibility = visibility
        binding.averageWritingScore.visibility = visibility
    }
}