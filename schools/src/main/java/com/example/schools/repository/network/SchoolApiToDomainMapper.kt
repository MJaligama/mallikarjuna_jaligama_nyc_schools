package com.example.schools.repository.network

import com.example.schools.models.api_models.SchoolDetailsApiModel
import com.example.schools.models.api_models.Schools
import com.example.schools.models.domain_models.DomainSchoolDetails
import com.example.schools.models.domain_models.DomainSchoolItem
import javax.inject.Inject

class SchoolApiToDomainMapper @Inject constructor() {

    fun mapSchoolListData(data: Schools): List<DomainSchoolItem> {
        data.map { school ->
            // Either set a default value or use the value from the API for fields that are nullable
            DomainSchoolItem(
                dbn = school.dbn,
                schoolName = school.school_name,
                overviewParagraph = school.overview_paragraph ?: "N/A",
                phoneNumber = school.phone_number ?: "N/A",
                schoolEmail = school.school_email ?: "N/A",
                stateCode = school.state_code ?: "N/A",
                totalStudents = school.total_students ?: "N/A",
                transfer = school.transfer ?: "N/A",
                website = school.website ?: "N/A",
                zip = school.zip ?: "N/A"
            )
        }.let {
            return it
        }
    }

    fun mapSchoolDetailsData(data: SchoolDetailsApiModel): DomainSchoolDetails {
        data.map { school ->
            DomainSchoolDetails(
                dbn = school.dbn,
                schoolName = school.school_name,
                totalNumberOfTestTakers = school.num_of_sat_test_takers,
                averageReadingScore = school.sat_critical_reading_avg_score,
                averageMathScore = school.sat_math_avg_score,
                averageWritingScore = school.sat_writing_avg_score
            )
        }.let {
            return it.first()
        }
    }
}