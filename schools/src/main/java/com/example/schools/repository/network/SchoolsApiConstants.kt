package com.example.schools.repository.network

object SchoolsApiConstants {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
}