package com.example.schools.repository

import com.example.schools.models.domain_models.DomainSchoolDetails
import com.example.schools.models.domain_models.DomainSchoolItem
import com.example.schools.repository.network.SchoolApiToDomainMapper
import com.example.schools.repository.network.SchoolsApiService
import com.example.schools.util.ApiResult
import javax.inject.Inject

interface SchoolsRepository {
    suspend fun getSchoolsList(): ApiResult<List<DomainSchoolItem>>
    suspend fun getSchoolDetails(id: String): ApiResult<DomainSchoolDetails>
}

class SchoolsRepositoryImpl @Inject constructor(
    private val schoolsApiService: SchoolsApiService,
    private val mapper: SchoolApiToDomainMapper
) : SchoolsRepository {

    override suspend fun getSchoolsList(): ApiResult<List<DomainSchoolItem>> {
        return try {
            val response = mapper.mapSchoolListData(schoolsApiService.getSchoolsList())
            if (response.isNotEmpty()) ApiResult.Success(response)
            else ApiResult.Failure("empty response")
        } catch (e: Throwable) {
            ApiResult.Failure(e)
        }
    }

    override suspend fun getSchoolDetails(id: String): ApiResult<DomainSchoolDetails> {
        return try {
            val response = mapper.mapSchoolDetailsData(schoolsApiService.getSchoolDetails(id))
            ApiResult.Success(response)
        } catch (e: Throwable) {
            ApiResult.Failure(e)
        }
    }
}