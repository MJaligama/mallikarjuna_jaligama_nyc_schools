package com.example.schools.repository.network

import com.example.schools.models.api_models.SchoolDetailsApiModel
import com.example.schools.models.api_models.SchoolDetailsApiModelItem
import com.example.schools.models.api_models.Schools
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApiService {

    @GET("s3k6-pzi2.json")
    suspend fun getSchoolsList(): Schools

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolDetails(@Query("dbn") dbn: String): SchoolDetailsApiModel
}