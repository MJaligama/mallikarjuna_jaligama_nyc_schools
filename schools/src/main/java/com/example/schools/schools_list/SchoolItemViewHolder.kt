package com.example.schools.schools_list

import androidx.recyclerview.widget.RecyclerView
import com.example.schools.databinding.SchoolListItemBinding
import com.example.schools.models.domain_models.DomainSchoolItem

class SchoolItemViewHolder(
    private val binding: SchoolListItemBinding,
    private val itemClickListener: SchoolItemAdapter.ItemClickListener
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(school: DomainSchoolItem) = binding.run {
        this.school = school
        this.listener = itemClickListener
        executePendingBindings()
    }
}