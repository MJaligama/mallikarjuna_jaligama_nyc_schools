package com.example.schools.schools_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.schools.models.domain_models.DomainSchoolItem
import com.example.schools.repository.SchoolsRepository
import com.example.schools.util.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolsListViewModel @Inject constructor(
    private val schoolsRepository: SchoolsRepository
) : ViewModel() {

    private val _schoolsList = MutableStateFlow<SchoolsListState>(SchoolsListState.Empty())
    val schoolsList: LiveData<SchoolsListState>
        get() = _schoolsList.asLiveData()

    init {
        initializeSchoolsScreen()
    }

    private fun initializeSchoolsScreen() {
        viewModelScope.launch {
            _schoolsList.value = SchoolsListState.Loading()
            when (val result = schoolsRepository.getSchoolsList()) {
                is ApiResult.Success -> onSuccess(result.value)
                is ApiResult.Failure -> _schoolsList.value = SchoolsListState.Failure("Fetching schools failed! error: ${result.error}")
            }
        }
    }

    private fun onSuccess(response: List<DomainSchoolItem>) {
        if (response.isEmpty()) _schoolsList.value = SchoolsListState.Empty()
        else _schoolsList.value = SchoolsListState.Success(response)
    }
}

sealed class SchoolsListState() {
    class Empty : SchoolsListState()
    class Loading : SchoolsListState()
    data class Success(val list: List<DomainSchoolItem>) : SchoolsListState()
    data class Failure(val message: String) : SchoolsListState()
}