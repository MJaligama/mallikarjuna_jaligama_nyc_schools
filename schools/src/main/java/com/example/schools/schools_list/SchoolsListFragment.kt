package com.example.schools.schools_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.schools.R
import com.example.schools.databinding.FragmentSchoolsListBinding
import com.example.schools.models.domain_models.DomainSchoolItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolsListFragment : Fragment(), SchoolItemAdapter.ItemClickListener {

    private val viewModel: SchoolsListViewModel by viewModels()

    private lateinit var binding: FragmentSchoolsListBinding
    private lateinit var adapter: SchoolItemAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSchoolsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().title = getString(R.string.schools_fragment_label)
        viewModel.apply {
            schoolsList.observe(viewLifecycleOwner) {
                it?.let { bindState(it) }
            }
        }
    }

    private fun bindState(state: SchoolsListState) {
        when (state) {
            is SchoolsListState.Loading -> {
                binding.progress.visibility = View.VISIBLE
                binding.errorText.visibility = View.GONE
            }
            is SchoolsListState.Success -> {
                binding.progress.visibility = View.GONE
                binding.errorText.visibility = View.GONE
                setAdapterData(state.list)
            }
            is SchoolsListState.Failure -> {
                binding.progress.visibility = View.GONE
                binding.errorText.visibility = View.VISIBLE
                binding.errorText.text = state.message
            }
            is SchoolsListState.Empty -> {
                binding.progress.visibility = View.GONE
                binding.errorText.visibility = View.VISIBLE
            }
        }
    }

    override fun onClickItem(view: View, schoolId: String) {
        findNavController().navigate(SchoolsListFragmentDirections.actionSchoolsListFragmentToSchoolTestDetailsFragment(schoolId))
    }

    private fun setAdapterData(schools: List<DomainSchoolItem>) {
        adapter = SchoolItemAdapter(schools, this@SchoolsListFragment)
        binding.recyclerView.adapter = adapter
    }
}