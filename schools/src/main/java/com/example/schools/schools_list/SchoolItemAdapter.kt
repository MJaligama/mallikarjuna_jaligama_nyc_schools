package com.example.schools.schools_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.schools.R
import com.example.schools.databinding.SchoolListItemBinding
import com.example.schools.models.domain_models.DomainSchoolItem

class SchoolItemAdapter(private val items: List<DomainSchoolItem>, private val itemClickListener: ItemClickListener) : RecyclerView.Adapter<SchoolItemViewHolder>() {

  interface ItemClickListener {
    fun onClickItem(view: View, schoolId: String)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolItemViewHolder =
    DataBindingUtil.inflate<SchoolListItemBinding>(LayoutInflater.from(parent.context), R.layout.school_list_item, parent, false).run {
      SchoolItemViewHolder(this, itemClickListener)
    }

  override fun getItemCount(): Int = items.size

  override fun onBindViewHolder(viewHolder: SchoolItemViewHolder, position: Int) {
    viewHolder.bind(items[position])
  }
}