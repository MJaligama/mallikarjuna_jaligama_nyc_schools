package com.example.schools.di

import com.example.schools.repository.network.SchoolsApiConstants
import com.example.schools.repository.network.SchoolsApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun providesNetworkService(retrofit: Retrofit.Builder): SchoolsApiService {
        return retrofit.baseUrl(SchoolsApiConstants.BASE_URL)
            .build()
            .create(SchoolsApiService::class.java)
    }
}